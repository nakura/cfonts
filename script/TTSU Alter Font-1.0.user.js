// ==UserScript==
// @name         TTSU Alter Font
// @namespace    http://tampermonkey.net/
// @version      1.0
// @description  Change the font of a ttsu Japanese text
// @author       Your name
// @match        https://reader.ttsu.app/*
// @grant        GM.xmlHttpRequest
// ==/UserScript==

(function() {
    'use strict';

    const curr_font = 5;

    // Font configuration in an array-like structure with explicit indices
    const fonts = [
        {   // Index 0
            index: 0,
            fontFamily: "EPSON 太行書体Ｂ",
            url: 'https://gitlab.com/nakura/cfonts/-/raw/master/fonts/EPGYOBLD.TTF?ref_type=heads&inline=false',
            weight: "regular"
        },
        {   // Index 1
            index: 1,
            fontFamily: "EPSON 正楷書体Ｍ",
            url: 'https://gitlab.com/nakura/cfonts/-/raw/master/fonts/EPKAISHO.TTF?ref_type=heads&inline=false',
            weight: "regular"
        },
        {   // Index 2
            index: 2,
            fontFamily: "EPSON 太角ゴシック体Ｂ",
            url: 'https://gitlab.com/nakura/cfonts/-/raw/master/fonts/EPKGOBLD.TTF?ref_type=heads&inline=false',
            weight: "regular"
        },
        {   // Index 3
            index: 3,
            fontFamily: "EPSON 教科書体Ｍ",
            url: 'https://gitlab.com/nakura/cfonts/-/raw/master/fonts/EPKYOUKA.TTF?ref_type=heads&inline=false',
            weight: "regular"
        },
        {   // Index 4
            index: 4,
            fontFamily: "EPSON 丸ゴシック体Ｍ",
            url: 'https://gitlab.com/nakura/cfonts/-/raw/master/fonts/EPMARUGO.TTF?ref_type=heads&inline=false',
            weight: "regular"
        },
        {   // Index 5
            index: 5,
            fontFamily: "EPSON 太丸ゴシック体Ｂ",
            url: 'https://gitlab.com/nakura/cfonts/-/raw/master/fonts/EPMGOBLD.TTF?ref_type=heads&inline=false',
            weight: "regular"
        },
        {   // Index 6
            index: 6,
            fontFamily: "EPSON 太明朝体Ｂ",
            url: 'https://gitlab.com/nakura/cfonts/-/raw/master/fonts/EPMINBLD.TTF?ref_type=heads&inline=false',
            weight: "regular"
        }
    ];

    // Function to load and apply a font by index
    function loadAndInjectFont(index) {
        const fontConfig = fonts.find(f => f.index === index);
        if (!fontConfig) {
            console.error("Font configuration not found for index:", index);
            return;
        }

        GM.xmlHttpRequest({
            method: "GET",
            url: fontConfig.url,
            responseType: "blob",
            onload: function(response) {
                const fontBlob = response.response;
                const blobUrl = URL.createObjectURL(fontBlob);

                const fontFaceRule = `
                    @font-face {
                        font-family: '${fontConfig.fontFamily}';
                        src: url('${blobUrl}') format('truetype');
                        font-weight: ${fontConfig.weight};
                    }
                    .book-content.svelte-u9j5ct.svelte-u9j5ct {
                        font-family: '${fontConfig.fontFamily}', sans-serif !important;
                        font-weight: ${fontConfig.weight} !important;
                    }
                `;

                const styleElement = document.createElement('style');
                styleElement.textContent = fontFaceRule;
                document.head.appendChild(styleElement);
                console.log('Font face injected and font family changed successfully for', fontConfig.fontFamily);
            },
            onerror: function(error) {
                console.error('Failed to load font for', fontConfig.fontFamily, error);
            }
        });
    }

    // Example usage - load the font by numerical index from the configuration
    loadAndInjectFont(curr_font);
})();